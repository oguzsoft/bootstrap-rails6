# How to add Bootstrap in Rails 6

#### Create Rails app 

```bash
rails new bootstrap-rails6
```
#### Open this folder an any code editor

#### Bundle 

```bash
bundle install
```

#### Add bootstrap, jquery and popper.js with using yarn

```bash
yarn add bootstrap jquery popper.js
```

#### add those lines in  ```config/webpack/enviroment.js```

```js
const { environment } = require('@rails/webpacker')
const webpack = require("webpack")

environment.plugins.append("Provide", new webpack.ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery',
  Popper: ['popper.js', 'default']
}))

module.exports = environment
```

#### Create a new folder 'src' in ```app/javascript``` and create a new file 'application.scss' in src

#### Add this line ```app/javascript/src/application.scss```

```scss
@import "~bootstrap/scss/bootstrap";
```

#### Update file  ```app/javascript/packs/application.js```

```js
require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

import "bootstrap"
require("src/application")
// or use import
// import "src/application"

document.addEventListener("turbolinks:load", () => {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
})  
```

#### In ```app/views/layouts/application.html.erb```
    * this line is changed in rails6.
    ```ruby
    <%= stylesheet_link_tag 'application', media: 'all', 'data-turbolinks-track': 'reload' %>
    ```

     Should be;

    ```ruby
    <%= stylesheet_pack_tag 'application', media: 'all', 'data-turbolinks-track': 'reload' %>
    ```
## Testing Bootstrap

 * Generate a dashboard controller

 ```bash
 rails g controller dashboard index
 ```

* Add Bootstrap's card in ```app/views/dashboard/index.html.erb```

```html
<div class="card" style="width: 18rem;">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>
```

